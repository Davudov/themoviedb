# themoviedb

Simple task project
- Language - Kotlin,
- Architecture - MVVM
- Network - Retrofit
- MultiThreading - Kotlin Coroutines
- DI - Koin