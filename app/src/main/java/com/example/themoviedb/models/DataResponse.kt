package com.example.themoviedb.models

import com.google.gson.annotations.SerializedName

data class DataResponse(
    @field:SerializedName("results")
    val data: List<Movie>,
    val page: Int,
)
