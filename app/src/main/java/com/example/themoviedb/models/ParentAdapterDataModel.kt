package com.example.themoviedb.models

sealed class ParentAdapterDataModel {
    object TopHeader : ParentAdapterDataModel()
    object HeaderPopularParentAdapter : ParentAdapterDataModel()
    object HeaderTopRatedParentAdapter : ParentAdapterDataModel()
}