package com.example.themoviedb.adapters.wrapper_adapter.view_holder

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.adapters.child_adapter.ChildAdapter
import com.example.themoviedb.databinding.ViewHorizontalWrapperBinding
import timber.log.Timber

class HorizontalWrapperViewHolder(
    private val binding: ViewHorizontalWrapperBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(adapter: ChildAdapter) {
        val context = binding.root.context
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.adapter = adapter
        Timber.d("Attach State HorizontalWrapperViewHolder setting new layoutManager $absoluteAdapterPosition")
    }
}