package com.example.themoviedb.adapters.child_adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.themoviedb.models.Movie

class DataDiffCallback : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }
    }
