package com.example.themoviedb.adapters.child_adapter.view_holder;

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.themoviedb.databinding.ListItemMovieBinding
import com.example.themoviedb.models.Movie
import kotlin.math.roundToInt

class ChildViewHolder(
    private val binding: ListItemMovieBinding,
    private val parent: ViewGroup
) :
    RecyclerView.ViewHolder(binding.root) {

    companion object {

        fun from(parent: ViewGroup): ChildViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemMovieBinding.inflate(layoutInflater, parent, false)
            return ChildViewHolder(binding, parent)
        }
    }

    fun bind(item: Movie) {
        binding.item = item
        Glide.with(parent.context)
            .load("https://image.tmdb.org/t/p/w500${item.backdropPath}")
            .override((240 * Resources.getSystem().displayMetrics.density).roundToInt())
            .into(binding.imageView)
        binding.executePendingBindings()
    }
}

