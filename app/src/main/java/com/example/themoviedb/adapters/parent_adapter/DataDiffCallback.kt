package com.example.themoviedb.adapters.parent_adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.themoviedb.models.ParentAdapterDataModel

class DataDiffCallback : DiffUtil.ItemCallback<ParentAdapterDataModel>() {
    override fun areItemsTheSame(oldModel: ParentAdapterDataModel, newModel: ParentAdapterDataModel): Boolean {
        return oldModel == newModel
    }

    override fun areContentsTheSame(oldModel: ParentAdapterDataModel, newModel: ParentAdapterDataModel): Boolean {
        return oldModel == newModel
    }
}