package com.example.themoviedb.adapters.parent_adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.adapters.AdapterConstants.HEADER
import com.example.themoviedb.adapters.AdapterConstants.HEADER_POPULAR_MOVIES
import com.example.themoviedb.adapters.AdapterConstants.HEADER_TOP_RATED_MOVIES
import com.example.themoviedb.adapters.parent_adapter.view_holders.HeaderPopularMoviesViewHolder
import com.example.themoviedb.adapters.parent_adapter.view_holders.HeaderTopRatedMoviesViewHolder
import com.example.themoviedb.adapters.parent_adapter.view_holders.TopHeaderViewHolder
import com.example.themoviedb.models.ParentAdapterDataModel


class ParentAdapter :
    ListAdapter<ParentAdapterDataModel, RecyclerView.ViewHolder>(DataDiffCallback()) {


    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ParentAdapterDataModel.HeaderPopularParentAdapter -> HEADER_POPULAR_MOVIES
            is ParentAdapterDataModel.HeaderTopRatedParentAdapter -> HEADER_TOP_RATED_MOVIES
            is ParentAdapterDataModel.TopHeader -> HEADER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {

            HEADER -> {
                TopHeaderViewHolder.from(parent)
            }
            HEADER_POPULAR_MOVIES -> {
                HeaderPopularMoviesViewHolder.from(parent)
            }
            HEADER_TOP_RATED_MOVIES -> {
                HeaderTopRatedMoviesViewHolder.from(parent)
            }

            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

}


