package com.example.themoviedb.adapters

object AdapterConstants {

    const val HEADER = 0
    const val HEADER_POPULAR_MOVIES = 1
    const val HEADER_TOP_RATED_MOVIES = 2
    const val DATA_TYPE_PARENT_MOVIES = 1

}