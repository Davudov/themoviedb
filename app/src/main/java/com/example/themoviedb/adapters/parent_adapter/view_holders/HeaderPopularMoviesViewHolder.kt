package com.example.themoviedb.adapters.parent_adapter.view_holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.R
import com.example.themoviedb.databinding.ListItemHeaderViewBinding

class HeaderPopularMoviesViewHolder(binding: ListItemHeaderViewBinding) :
    RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun from(parent: ViewGroup): HeaderPopularMoviesViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemHeaderViewBinding.inflate(layoutInflater, parent, false)
            binding.title.text = parent.context.getString(R.string.popular_movies_title)
            return HeaderPopularMoviesViewHolder(binding)
        }
    }
}