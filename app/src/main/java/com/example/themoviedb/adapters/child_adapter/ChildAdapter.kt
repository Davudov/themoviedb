package com.example.themoviedb.adapters.child_adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.adapters.AdapterConstants.DATA_TYPE_PARENT_MOVIES
import com.example.themoviedb.adapters.child_adapter.view_holder.ChildViewHolder
import com.example.themoviedb.models.Movie


class ChildAdapter : PagingDataAdapter<Movie, RecyclerView.ViewHolder>(
    DataDiffCallback()
) {


    override fun getItemViewType(position: Int): Int {
        return DATA_TYPE_PARENT_MOVIES
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ChildViewHolder) {
            getItem(position)?.let { holder.bind(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ChildViewHolder.from(parent)
    }

    }


