package com.example.themoviedb.adapters.parent_adapter.view_holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.R
import com.example.themoviedb.databinding.ListItemHeaderViewBinding

class TopHeaderViewHolder(binding: ListItemHeaderViewBinding) :
    RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun from(parent: ViewGroup): TopHeaderViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemHeaderViewBinding.inflate(layoutInflater, parent, false)
            binding.title.text = parent.context.getString(R.string.header_text)
            return TopHeaderViewHolder(binding)
        }
    }
}
