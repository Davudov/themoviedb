package com.example.themoviedb.adapters.wrapper_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.adapters.child_adapter.ChildAdapter
import com.example.themoviedb.adapters.wrapper_adapter.view_holder.HorizontalWrapperViewHolder
import com.example.themoviedb.databinding.ViewHorizontalWrapperBinding

class HorizontalWrapperAdapter(
    private val adapter: ChildAdapter
) : RecyclerView.Adapter<HorizontalWrapperViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HorizontalWrapperViewHolder {
        return HorizontalWrapperViewHolder(
            ViewHorizontalWrapperBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemViewType(position: Int): Int {
        return 9
    }

    override fun onBindViewHolder(holder: HorizontalWrapperViewHolder, position: Int) {
        holder.bind(adapter)
    }

    override fun getItemCount(): Int = 1

}