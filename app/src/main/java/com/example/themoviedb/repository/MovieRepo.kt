package com.example.themoviedb.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource
import com.example.themoviedb.models.DataResponse
import com.example.themoviedb.models.Movie
import com.example.themoviedb.network.NetworkConstants
import com.example.themoviedb.network.services.MovieServices

class MovieRepo(private val movieServices: MovieServices) {

    private val _dataInfo: MutableLiveData<Pair<DataResponse?, Status>> =
        MutableLiveData(Pair(null, Status.IDLE))

    val dataInfo: LiveData<Pair<DataResponse?, Status>> = _dataInfo


    suspend fun getPopularMovies(
        prevKey: Int?,
        nextKey: Int,
        pageNum: Int
    ): PagingSource.LoadResult<Int, Movie> {
        _dataInfo.postValue(Pair(null, Status.LOADING))
        return try {
            val res =
                movieServices.getPopularMovies(NetworkConstants.API_KEY, pageNum.toString()).await()
            if (res.data.isNotEmpty()) {
                PagingSource.LoadResult.Page(res.data, prevKey, nextKey)
            } else {
                PagingSource.LoadResult.Error(Throwable("result data is empty"))

            }
        } catch (e: Exception) {
            PagingSource.LoadResult.Error(e)
        } finally {
            _dataInfo.postValue(Pair(null, Status.LOADING_COMPLETED))
        }

    }
    suspend fun getTopRatedMovies(
        prevKey: Int?,
        nextKey: Int,
        pageNum: Int
    ): PagingSource.LoadResult<Int, Movie> {
        _dataInfo.postValue(Pair(null, Status.LOADING))
        return try {
            val res =
                movieServices.getTopRatedMovies(NetworkConstants.API_KEY, pageNum.toString()).await()
            if (res.data.isNotEmpty()) {
                PagingSource.LoadResult.Page(res.data, prevKey, nextKey)
            } else {
                PagingSource.LoadResult.Error(Throwable("result data is empty"))

            }
        } catch (e: Exception) {
            PagingSource.LoadResult.Error(e)
        } finally {
            _dataInfo.postValue(Pair(null, Status.LOADING_COMPLETED))
        }

    }
}