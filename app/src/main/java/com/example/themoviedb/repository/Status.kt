package com.example.themoviedb.repository

enum class Status { LOADING, LOADING_COMPLETED,  IDLE }
