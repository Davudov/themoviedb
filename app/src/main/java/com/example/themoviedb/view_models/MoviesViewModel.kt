package com.example.themoviedb.view_models

import androidx.lifecycle.*
import androidx.paging.*
import com.example.themoviedb.models.Movie
import com.example.themoviedb.paging.PopularMoviesDataSource
import com.example.themoviedb.paging.TopRatedMoviesDataSource
import com.example.themoviedb.repository.MovieRepo
import com.example.themoviedb.repository.Status

class MoviesViewModel(movieRepo: MovieRepo, private val popularMoviesDataSource: PopularMoviesDataSource, private val topRatedMoviesDataSource: TopRatedMoviesDataSource) : ViewModel() {
    private val isLoading = MutableLiveData(false)

     private val dataInfo = movieRepo.dataInfo

    val progressVisible: LiveData<Boolean> = Transformations.switchMap(dataInfo) {
        isLoading.postValue(it.second == Status.LOADING)
        isLoading
    }


    fun getPopularMoviesPaged() : LiveData<PagingData<Movie>>{
        val config = PagingConfig(
            pageSize = 10
        )
        val pager = Pager(config = config, pagingSourceFactory = { popularMoviesDataSource })
        return pager.liveData.cachedIn(viewModelScope)
         //persist the data on viewModel configuration changes
    }

    fun getTopRatedMoviesPaged(): LiveData<PagingData<Movie>> {
        val config = PagingConfig(
            pageSize = 10
        )
        val pager = Pager(config = config, pagingSourceFactory = { topRatedMoviesDataSource })
        return pager.liveData.cachedIn(viewModelScope)
    }
}