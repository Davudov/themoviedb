package com.example.themoviedb.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.themoviedb.models.Movie
import com.example.themoviedb.repository.MovieRepo

class PopularMoviesDataSource(private val movieRepo: MovieRepo) : PagingSource<Int, Movie>() {

    private val initialLoadSize = 1

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        val position = params.key ?: initialLoadSize
        val prevKey = null
        val nextKey = position + 1
        return try {
            movieRepo.getPopularMovies(prevKey,nextKey,position)
        }catch (e:Exception){
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition
    }
}