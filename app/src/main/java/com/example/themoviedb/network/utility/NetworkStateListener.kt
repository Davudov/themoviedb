package com.example.themoviedb.network.utility

import android.content.Context
import androidx.paging.PagingData
import com.example.themoviedb.models.Movie

interface NetworkStateListener {
    fun onReloadTopMovies(list: PagingData<Movie>)
    fun onReloadPopularMovies(list: PagingData<Movie>)
    fun onNetworkLost(context: Context)
}