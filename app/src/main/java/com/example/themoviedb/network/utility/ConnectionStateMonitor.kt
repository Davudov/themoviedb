package com.example.themoviedb.network.utility

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.example.themoviedb.view_models.MoviesViewModel
import kotlinx.coroutines.launch

class ConnectionStateMonitor(
    private val activity: FragmentActivity,
    private val lifecycleOwner: LifecycleOwner,
    private val networkStateListener: NetworkStateListener,
    val viewModel: MoviesViewModel
) :
    ConnectivityManager.NetworkCallback() {
    private val networkRequest: NetworkRequest = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .build()

    fun enable(): Boolean {
        val connectivityManager =
            activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.registerNetworkCallback(networkRequest, this)


        var result = false
        activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
        }
        return result
    }


    override fun onAvailable(network: Network) {
        Toast.makeText(activity,"Network is available started to load data",Toast.LENGTH_LONG).show()
        lifecycleOwner.lifecycleScope.launch {
            viewModel.getPopularMoviesPaged().observe(lifecycleOwner, { list ->
                networkStateListener.onReloadPopularMovies(list)
            })
        }
        lifecycleOwner.lifecycleScope.launch {
            viewModel.getTopRatedMoviesPaged().observe(lifecycleOwner, { list ->
                networkStateListener.onReloadTopMovies(list)
            })
        }
    }

    override fun onUnavailable() {
        Toast.makeText(activity,"No Internet connection",Toast.LENGTH_LONG).show()
        super.onUnavailable()
    }


    override fun onLost(network: Network) {
        networkStateListener.onNetworkLost(activity)
        super.onLost(network)
    }
}
