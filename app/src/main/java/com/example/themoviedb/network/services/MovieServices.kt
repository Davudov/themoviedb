package com.example.themoviedb.network.services

import com.example.themoviedb.models.DataResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieServices {

    @GET("movie/popular")
     fun getPopularMovies(
        @Query("api_key") apiKey: String,
        @Query("page") page: String
    ): Deferred<DataResponse>

    @GET("movie/top_rated")
     fun getTopRatedMovies(
        @Query("api_key") apiKey: String,
        @Query("page") page: String
    ): Deferred<DataResponse>

}