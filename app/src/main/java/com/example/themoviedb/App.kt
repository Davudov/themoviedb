package com.example.themoviedb

import android.app.Application
import com.example.themoviedb.network.NetworkConstants.BASE_URL
import com.example.themoviedb.network.services.MovieServices
import com.example.themoviedb.paging.PopularMoviesDataSource
import com.example.themoviedb.paging.TopRatedMoviesDataSource
import com.example.themoviedb.repository.MovieRepo
import com.example.themoviedb.view_models.MoviesViewModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        Timber.d("onCreate")

        initKoin()
        super.onCreate()
    }

    private fun initKoin() {
        val networkModule = module {
            single { provideOkHttpClient() }
            single { provideRetrofit(get()) }
            single { provideMovieService(get()) }
        }

        val repoModule = module {
            single { MovieRepo(get()) }
            single { PopularMoviesDataSource(get()) }
            single { TopRatedMoviesDataSource(get()) }
        }

        val viewModelModule = module {
            viewModel {
                MoviesViewModel(get(), get(), get())
            }
        }
        startKoin {
            androidContext(this@App)
            modules(listOf(networkModule,repoModule,viewModelModule))
        }


    }


    private fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
        .build()

    private fun provideRetrofit(okHttpClient: OkHttpClient) = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

    private fun provideMovieService(retrofit: Retrofit): MovieServices =
        retrofit.create(MovieServices::class.java)

}