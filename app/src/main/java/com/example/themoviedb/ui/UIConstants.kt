package com.example.themoviedb.ui

object UIConstants {
    const val RECYCLER_VIEW_LAST_STATE_KEY: String = "RECYCLERVIEW_LAST_STATE_KEY"
}