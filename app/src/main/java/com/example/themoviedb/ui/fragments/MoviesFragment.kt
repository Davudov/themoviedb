package com.example.themoviedb.ui.fragments

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.adapters.child_adapter.ChildAdapter
import com.example.themoviedb.adapters.parent_adapter.ParentAdapter
import com.example.themoviedb.adapters.wrapper_adapter.HorizontalWrapperAdapter
import com.example.themoviedb.databinding.FragmentMoviesBinding
import com.example.themoviedb.models.Movie
import com.example.themoviedb.models.ParentAdapterDataModel
import com.example.themoviedb.network.utility.ConnectionStateMonitor
import com.example.themoviedb.network.utility.NetworkStateListener
import com.example.themoviedb.ui.UIConstants.RECYCLER_VIEW_LAST_STATE_KEY
import com.example.themoviedb.ui.compound_views.DeceleratedRecyclerView
import com.example.themoviedb.view_models.MoviesViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class MoviesFragment : Fragment(), NetworkStateListener {

    private lateinit var binding: FragmentMoviesBinding


    private lateinit var connectionStateMonitor: ConnectionStateMonitor

    private lateinit var concatAdapter: ConcatAdapter

    private lateinit var moviesAdapter: ChildAdapter
    private lateinit var topRatedMoviesAdapter: ChildAdapter
    private val viewModel: MoviesViewModel by viewModel()

    private lateinit var horizontalWrapperPopularMoviesAdapter: HorizontalWrapperAdapter
    private lateinit var horizontalWrapperTopRatedAdapter: HorizontalWrapperAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentMoviesBinding.inflate(inflater)
        Timber.d("lifecycle event onCreateView")

        moviesAdapter = ChildAdapter()
        topRatedMoviesAdapter = ChildAdapter()

        horizontalWrapperPopularMoviesAdapter = HorizontalWrapperAdapter(moviesAdapter)
        horizontalWrapperTopRatedAdapter = HorizontalWrapperAdapter(topRatedMoviesAdapter)


        connectionStateMonitor =
            ConnectionStateMonitor(requireActivity(), viewLifecycleOwner, this, viewModel)
        if (!connectionStateMonitor.enable()) {
            Toast.makeText(requireContext(), "No Internet connection", Toast.LENGTH_LONG).show()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val headerAdapter = ParentAdapter()
        val headerSecondAdapter = ParentAdapter()

        val list = arrayListOf<ParentAdapterDataModel>()
        list.add(ParentAdapterDataModel.TopHeader)
        list.add(ParentAdapterDataModel.HeaderPopularParentAdapter)

        headerAdapter.submitList(list)
        val list1 = arrayListOf<ParentAdapterDataModel>()
        list1.add(ParentAdapterDataModel.HeaderTopRatedParentAdapter)
        headerSecondAdapter.submitList(list1)

        val config = ConcatAdapter.Config.Builder().apply {
            setIsolateViewTypes(false)
        }.build()

        concatAdapter = ConcatAdapter(
            config,
            headerAdapter,
            horizontalWrapperPopularMoviesAdapter,
            headerSecondAdapter,
            horizontalWrapperTopRatedAdapter
        )

        binding.parentRecycler.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        binding.parentRecycler.adapter = concatAdapter

        binding.parentRecycler.addOnChildAttachStateChangeListener(recyclerViewAttachListener)
    }


    private val recyclerViewAttachListener = object :
        RecyclerView.OnChildAttachStateChangeListener {

        override fun onChildViewAttachedToWindow(view: View) {
            if (view is DeceleratedRecyclerView) {
                val pos = binding.parentRecycler.getChildLayoutPosition(view)
                Timber.d("Attach State onChildViewAttachedToWindow position $pos ")
                val key = "child${pos}_recyclerview_state"
                val lastState = requireActivity().intent.getParcelableExtra<Parcelable>(key)
                view.layoutManager!!.onRestoreInstanceState(
                    lastState
                )
            }
        }

        override fun onChildViewDetachedFromWindow(view: View) {
            if (view is DeceleratedRecyclerView) {
                val pos = binding.parentRecycler.getChildLayoutPosition(view)
                Timber.d("Attach State onChildViewDetachedFromWindow position $pos ")
                val key = "child${pos}_recyclerview_state"
                val parcelable = view.layoutManager!!.onSaveInstanceState()
                requireActivity().intent.putExtra(key, parcelable)
            }
        }

    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(
            RECYCLER_VIEW_LAST_STATE_KEY,
            binding.parentRecycler.layoutManager!!.onSaveInstanceState()
        )

        val children = binding.parentRecycler.children

        children.filter { it is DeceleratedRecyclerView }.map { it as DeceleratedRecyclerView }
            .forEach { view ->
                val pos = binding.parentRecycler.getChildLayoutPosition(view)
                Timber.d("onSaveInstanceState Attach State onChildViewDetachedFromWindow position $pos ")
                val key = "child${pos}_recyclerview_state"
                val parcelable = view.layoutManager!!.onSaveInstanceState()
                requireActivity().intent.putExtra(key, parcelable)
            }
    }
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState ?: return
        binding.parentRecycler.layoutManager!!.onRestoreInstanceState(
            savedInstanceState.getParcelable(
                RECYCLER_VIEW_LAST_STATE_KEY
            )
        )
    }


    override fun onReloadTopMovies(list: PagingData<Movie>) {
        viewLifecycleOwner.lifecycleScope.launch {
            topRatedMoviesAdapter.submitData(lifecycle, list)
        }
    }

    override fun onReloadPopularMovies(list: PagingData<Movie>) {
        viewLifecycleOwner.lifecycleScope.launch {
            moviesAdapter.submitData(lifecycle, list)
        }
    }

    override fun onNetworkLost(context: Context) {
        Timber.d("Network onNetworkLost")
        Toast.makeText(context, "No Internet connection", Toast.LENGTH_LONG)
            .show()
    }


}
