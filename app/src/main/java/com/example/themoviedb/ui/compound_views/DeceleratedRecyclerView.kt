package com.example.themoviedb.ui.compound_views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

class DeceleratedRecyclerView : RecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attr: AttributeSet) : super(context, attr)
    constructor(context: Context, attr: AttributeSet, defStyleAttr: Int) : super(
        context,
        attr,
        defStyleAttr
    )

    override fun onNestedFling(
        target: View?,
        velocityX: Float,
        velocityY: Float,
        consumed: Boolean
    ): Boolean {
        Timber.d("onNestedFling velocityX $velocityX velocityY $velocityY consumed? $consumed")
        return super.onNestedFling(target, velocityX, velocityY, consumed)

    }

    override fun fling(velocityX: Int, velocityY: Int): Boolean {
        Timber.d("fling velocityX $velocityX velocityY $velocityY ")
        val slowedFlingGestureVelocityY = velocityY-velocityY/2

        val slowedFlingGestureVelocityX = velocityX-velocityX/1.7

        Timber.d("fling result velocityX $slowedFlingGestureVelocityX velocityY $slowedFlingGestureVelocityY ")

        return super.fling(slowedFlingGestureVelocityX.toInt(), slowedFlingGestureVelocityY)
    }

}